#!/usr/bin/env bash

set -x

nix build

sudo rm -rf ~/.vim

cp -a result/. ~/.vim/

if [[ ! -e /root/.vim ]]; then
    sudo rm -rf /root/.vim
    sudo ln -s ~/.vim /root/
fi
