{
  description = "Flake for building contents of .vim/";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs";
    vim-nix = {
      url = "github:LnL7/vim-nix";
      flake = false;
    };
  };

  outputs = { self, flake-utils, nixpkgs, vim-nix }:
    (flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let

        pkgs = import nixpkgs { inherit system; };

        cp-dirs = with builtins;
          package: dirs:
          (concatStringsSep "\n"
            (map (dir: "cp -r ${package}/${dir} $out") dirs));

        vim-nix-setup = pkgs.writeShellScript "vim-nix-setup.sh" ''
          ${cp-dirs vim-nix [ "ftdetect" "ftplugin" "indent" "syntax" ]}
        '';

      in {

        defaultPackage = pkgs.stdenv.mkDerivation {
          name = "vim-config";
          src = ./.;
          buildInputs = with pkgs; [ coreutils ];
          phases = [ "unpackPhase" "buildPhase" "installPhase" ];
          buildPhase = "";
          installPhase = ''
            mkdir -p $out
            ${vim-nix-setup}
          '';
        };

      }));
}
