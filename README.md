# Vim Configuration

This configuration is built using nix and requires nix with the experimentals flake commands enabled.

To build and copy configuration to user and root `.vim` directories

    ./deploy.sh
